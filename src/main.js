import Vue from 'vue'
import App from './App.vue'

import VueScrollmagic from 'vue-scrollmagic'

Vue.config.productionTip = false

import store from "./store/index.js"
import router from "./router/index.js"

Vue.use(VueScrollmagic)

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
