import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import HelloWorld from "../components/HelloWorld"

import Elementary1 from "../components/Elementary1"
import Elementary2 from "../components/Elementary2"
import Elementary3 from "../components/Elementary3"

import Middle1 from "../components/Middle1"

const routes = [
    { path: '/', component: HelloWorld },
    { path: '/e1', component: Elementary1 },
    { path: '/e2', component: Elementary2 },
    { path: '/e3', component: Elementary3 },
    { path: '/m1', component: Middle1 },
  ]

  const router = new VueRouter({
    routes 
  })
  

  export default router;