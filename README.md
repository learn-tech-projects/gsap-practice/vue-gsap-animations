# vue-gsap-animations

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Elemantary3 comments
Element:в данном элементы сделана, наконец-то, связка gsap  и scrollmahic
Проблемы до этого были из за того, что у GSAP было обновление, а scroll-magic, как я сумел понять еще не обновился

Так что для того, чтобы все работало должны быть выполнены следующие условия:
```
gsap: 2.1.3
scrollmagic:1.2.0
```

О GSAP ( за основу берется документация версии v2 ссылка: [GSAP v2](https://greensock.com/docs/v2 ):
* TweenMax  - данный элемент просто помогает запускать определенные изменения. То есть код ниже сразу после загрузки выполнит изменения 
```javascript
    TweenMax.from('#circle1',0.65,{
                    scaleX:0,
                    yPercent: 100,
                    ease: Back.easeOut,
                    delay:.35
                })
```
* TimelineMax - запускается на сразу. По крайней мере именно во второй версии GSAP. То есть тут можно комбинировать определенные части и после запускать, к примеру, через ScrollMagic. Пример кода ниже:
```javascript
    let tl = new TimelineMax();

        tl
            .to('#circle2',0.65,{
                x:500,
                rotation:180,
            },)
            .to('#circle3',0.35,{
                x:'-=200',
                rotation:'-=180',
            })
        
        let tl2 = new TimelineMax()

        tl2
        .from("#circle4",0.35,{
            scaleX:0,
            yPercent: 100,
            ease: Back.easeOut,
        })

        const scene1 = this.$scrollmagic
        .scene({
            triggerElement:'#trigger2',
            triggerHook:0,
            // duration:600 *Если есть данная строчка, то duration  в самом TimeLineMax изменяется и адаптируется под этот duration, так что наверное в большинстве его лучше закомментироват
        })
        .setTween(tl)
        
        const scene2 = this.$scrollmagic
        .scene({
            triggerElement:'#trigger4',
            triggerHook:0,
        })
        .setTween(tl2)

        this.$scrollmagic.addScene([
            scene1,
            scene2,
        ])
```

О взаимодействия с ScrollMagic:
* Тут все просто так, как создается к примеру TiemlimeMax элемет и просто добавляется в scene через setTween